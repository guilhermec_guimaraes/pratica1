/*
* UTFPR Dois Vizinhos 
* Disciplina: Programação Concorrente e Distribuida
* 
* Descricao: Programa exemplo de Modelo Cliente/Servidor
* Autor: Guilherme
* Ultima atualizacao: 11/08/2017
*
 */
package utfpr.dv.sd.aplicado1.guilherme;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HttpClient {

    public HttpClient() {
        super();
    }
    
    public void setQualquerURL(String URL) {
        this.minhaURL = URL;
        this.setURL();
    }
    
    public void setURL() {
        String URL = this.minhaURL + "/get";
        try {

            CloseableHttpClient httpclient = HttpClients.createDefault();

            try {

                HttpGet httpGet = new HttpGet(URL);
                CloseableHttpResponse response1 = httpclient.execute(httpGet);
                // The underlying HTTP connection is still held by the response object
                // to allow the response content to be streamed directly from the network socket.
                // In order to ensure correct deallocation of system resources
                // the user MUST call CloseableHttpResponse#close() from a finally clause.
                // Please note that if response content is not fully consumed the underlying
                // connection cannot be safely re-used and will be shut down and discarded
                // by the connection manager.
                try {
                    System.out.println(response1.getStatusLine());
                    HttpEntity entity1 = response1.getEntity();
                    // do something useful with the response body
                    // and ensure it is fully consumed
                    EntityUtils.consume(entity1);
                } finally {
                    response1.close();
                }//

            } finally {
                httpclient.close();
            }//

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }
    
    private String minhaURL = "";

    public void post(String username, String password) {

        String URL = this.minhaURL + "/post";
        try {

            CloseableHttpClient httpclient = HttpClients.createDefault();

            try {

                HttpPost httpPost = new HttpPost(URL);
                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("username", username));
                nvps.add(new BasicNameValuePair("password", password));
                httpPost.setEntity(new UrlEncodedFormEntity(nvps));
                CloseableHttpResponse response2 = httpclient.execute(httpPost);

                try {
                    System.out.println(response2.getStatusLine());
                    HttpEntity entity2 = response2.getEntity();
                    // do something useful with the response body
                    // and ensure it is fully consumed
                    EntityUtils.consume(entity2);
                } finally {
                    response2.close();
                }

            } finally {
                httpclient.close();
            }//

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }

}
