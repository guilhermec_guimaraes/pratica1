/*
* UTFPR Dois Vizinhos 
* Disciplina: Programação Concorrente e Distribuida
*
* Descricao: Programa exemplo de Modelo Cliente/Servidor
* Autor: Guilherme
* Ultima atualizacao: 11/08/2017
*
* ----------------
* Exercicios: 
* 1) Modifique as propriedades desse projeto para os seguintes valores:
*    - groupid: utfpr.dv.sd.aplicado1.SEU_NOME
*    - ArtifactID: aplicado1
*    - Nome: "SistemasDistribuidos :: Aplicado :: 1" (sem aspas duplas)
*    - Modifique o nome da pasta utfpr.dv.sd.pratica para utfpr.dv.sd.aplicado1.SEU_NOME
*    - Modifique o nome da classe Aplicado1 para Aplicado1
* 2) Implemente um metodo SET na classe HttpClient para atribuir qualquer URL
* 3) Implemente um metodo publico post() na classe HttpClient para atribuir 
*    os parametros 'URL', 'username' e 'password'
* 4) Publique o seu projeto em um repositorio online com acesso publico
* 5) Informe a URL do seu projeto que foi publicado
* ----------------
*/

// https://guilhermec_guimaraes@bitbucket.org/guilhermec_guimaraes/pratica1.git
import utfpr.dv.sd.aplicado1.guilherme.HttpClient;
        
public class Aplicado1 {
    
    public Aplicado1(){        
        
        HttpClient objeto = new HttpClient();        

        objeto.setQualquerURL("http://httpbin.org");
        objeto.setURL();
        
        objeto.post("vip","secret"); //URL, username, password
    }
    
    public static void main(String [] args){
        
        Aplicado1 p = new Aplicado1();
                
    }
    
}
